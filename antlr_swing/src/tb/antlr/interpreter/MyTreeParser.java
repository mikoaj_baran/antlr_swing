package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols globals = new GlobalSymbols();

	

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    protected void drukuj(Integer tmp_int) {
        System.out.println(tmp_int.toString());
    }
    
	protected Integer power(Integer base, Integer exponent) {
		return (int) Math.pow(base, exponent);
	}

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
    public void dodaj_zmienna(String name) {
    		globals.newSymbol(name);
    }
    
    public void ustaw_zmienna(String name, Integer value) {
    		globals.setSymbol(name, value);
    }
    
    public Integer wartosc(String name) {
    	return globals.getSymbol(name);
    }
}
